module ThemesHelper
	def set_theme_as_current_theme_for(params = {})
		user ||= params[:user]
		theme ||= params[:theme]
		return false if theme.nil? || user.nil?

		user.user_account[:current_theme] ||= theme.id
		user.save

		true
	end

	def populate_free_themes_for(params = {})
		user ||= params[:user]
		themes = params[:themes] || []
		return false if user.nil? || themes.blank?

		themes.each do |theme|
			user.user_themes.find_or_create_by(user_id: user.id, theme_id: theme.id)
		end
		true
	end

end
