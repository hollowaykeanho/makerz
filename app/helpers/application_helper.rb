module ApplicationHelper
	def save_back_url
		# Get out if referer URL is not available
		return if request.referer.nil? || request.referer.blank?

		# Save back_url based on condition
		if session[:back_url].nil? && URI(request.referer).path != request.fullpath
			session[:back_url] = URI(request.referer).path
		end
	end

	def back_url
		session[:back_url] || user_accounts_path
	end

	def clear_back_url(option = {})
		by_force = option[:by_force] || false
		session[:back_url] = nil if request.fullpath == session[:back_url] || by_force
	end

	def has_no_contents?(object)
		return true if object.nil? || object.blank?
		false
	end

	def default_img_path
		"https://lh3.googleusercontent.com/daoXDa-4v-mhLX8HziY9dN3L9IMwwrKqVuugW4WqBVG3=w638-h359-no"
	end
end
