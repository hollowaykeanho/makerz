module Users::AccountsHelper
	def validate_user
    redirect_to user_accounts_show_path unless current_user.id == UserAccount.find(params[:id]).user_id
  end
end
