module Users::ProfilesHelper
	def retrieve_user_from_subdomain
		@user_account = UserAccount.where(subdomain: request.subdomain).first
	end

	def select_theme
		return nil if @user_account.nil?
		return "hosting_store" if @user_account.current_theme.nil?
		theme = Theme.find_by(id: @user_account.current_theme).name
	end

end
