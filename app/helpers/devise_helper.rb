module DeviseHelper
	def devise_error_messages!
		return "" if defined?(resource).nil? || resource.errors.empty?

		messages = resource.errors.full_messages.map { |msg| msg + "\n\n" }.join
		sentence = I18n.t("errors.messages.not_saved",
											:count => resource.errors.count,
											:resource => resource.class.model_name.human.downcase)

		html = <<-HTML
			<div id="flash-message" data-msg="#{messages}", data-notice="warning"></div>
		HTML

		html.html_safe
	end
end