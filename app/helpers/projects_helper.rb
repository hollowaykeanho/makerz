module ProjectsHelper
	def validate_project_existance
		@project = Project.find_by(id: params[:id])
		redirect_to projects_url, :alert => "No such project." if @project.nil?
	end

	def validate_owner
		unless @project.user.id == current_user.id
			redirect_to projects_url, alert: "Seems like you aren't the owner of the project.\n Let us know if you found any bugs. =)"
		end
	end

	def validate_user_confirmation
		redirect_to user_accounts_path, alert: "I'm sorry you have to confirm your account email in order to use this feature." unless current_user.confirmation_token.nil?
	end
end
