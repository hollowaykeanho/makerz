module Themes::SelectorsHelper

	def extract_user_themes
		themes = current_user.themes.all
		themes = nil if themes.blank?
		themes
	end

	def extract_user_account
		@user_account = current_user.user_account
	end

end
