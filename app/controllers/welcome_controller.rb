class WelcomeController < ApplicationController
	protect_from_forgery
	before_filter :authenticate_user!, only: [:first_time_done]
	theme "default", only: [:index]

  def index
		redirect_to user_accounts_path if current_user
  end

  def email
  end

  def save_email
  end

  def first_time_done
		@subdomain_link = current_user.user_account[:subdomain]

		unless @subdomain_link.nil? || @subdomain_link.blank?
			@subdomain_link = "http://" + @subdomain_link + ".makerz.io"
		end

		session[:first_time] = nil
  end
end
