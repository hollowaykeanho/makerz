class SearchesController < ApplicationController
	protect_from_forgery

  def project_search
  	@filterrific = initialize_filterrific(
      Project,
      params[:filterrific], select_options: {
        sorted_by: Project.options_for_sorted_by,
        with_tag_id: Tag.options_for_select
      }
    ) or return

    # Process output for views
    @page_title = "Search Project"
    @page_description = "Search any project through Makerz tag or title."
    @projects = @filterrific.find.page(params[:page])

    respond_to do |format|
      format.html
      format.js
    end
  end

end
