class UsersController < ApplicationController
	protect_from_forgery
	before_filter :authenticate_user!

  def edit
    save_back_url

    # Process output for views
    @page_title = "Managing User Privacy"
    @page_description = "This is Makerz.io User Privacy Management, Accessible after Login."

    @user = current_user
    @back_url = back_url
  end

  def update
    @user = User.find(current_user.id)
    if @user.update_with_password(user_params)
      sign_in @user, :bypass => true
      notification = { notice: "Details updated!" }
    else
      notification = { alert: "Invalid Password.\n Please enter again." }
    end
    redirect_to edit_user_path, notification
  end

  private

  def user_params
    params.require(:user).permit(:password, :password_confirmation, :current_password)
  end
end
