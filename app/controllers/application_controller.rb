class ApplicationController < ActionController::Base
  include ApplicationHelper

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  after_filter :store_location
  helper_method :current_order

	def store_location
    session[:previous_url] = request.fullpath unless request.fullpath =~ /(\/sign|\/confirmation)/
	end

  def current_order
    if session[:order_id].nil?
      current_user.orders.new
    elsif current_user.orders.find(session[:order_id]).order_status_id == 3
      current_user.orders.find(session[:order_id]).destroy
      session[:order_id] = nil
      current_user.orders.new

    elsif !session[:order_id].nil?
      current_user.orders.find(session[:order_id])
    end
  end

end
