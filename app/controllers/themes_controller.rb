class ThemesController < ApplicationController
	protect_from_forgery
	before_action :authenticate_user!

	def index
		save_back_url
		themes = Theme.all


		# Process output for views
		@page_title = "Shop for Theme"
		@page_description = "Shop New Themes for Your Makerz Profile. Accessible after Login."

		@js_custom_turbolink = { 'turbolinks-track' => true, "turbolinks-eval" => false }
		@user_themes = current_user.themes
		@order_item = current_order.order_items.new
		@themes = themes - @user_themes
		@back_url = back_url
	end
end
