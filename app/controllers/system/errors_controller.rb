class System::ErrorsController < ApplicationController
	def error_404
		# Prepare for Views
		@page_title = "Error 404"
		@page_description = "Makerz.io Error 404 page."
	end

	def error_422
		# Prepare for Views
		@page_title = "Error 422"
		@page_description = "Makerz.io Error 422 page."
	end

	def error_500
		# Prepare for Views
		@page_title = "Error 500"
		@page_description = "Makerz.io Error 500 page."
	end

	def error_no_such_project
		# Prepare for Views
		@page_title = "Error No Such Project"
		@page_description = "Makerz.io no project found page."
	end

	def no_such_profile
		# Prepare for Views
		@page_title = "No Such Profile"
		@page_description = "Makerz.io couldn't find the profile you need."
	end
end
