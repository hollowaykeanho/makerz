class Themes::SelectorsController < ApplicationController
  include ApplicationHelper
  include Themes::SelectorsHelper
  protect_from_forgery
  before_action :authenticate_user!

  # def index
  # end

  # def show
  # end

  # def new
  # end

  # def create
  # end

  def edit
    save_back_url

    # Output for View Files
    @page_title = "Select Theme"
    @page_description = "Select the Makerz Profile Theme for your profile. Available after login."

    @themes = extract_user_themes
    @user_account = extract_user_account
    @back_url = back_url
  end

  # def update
  # end

  # def destroy
  # end
end
