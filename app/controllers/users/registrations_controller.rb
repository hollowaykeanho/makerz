class Users::RegistrationsController < Devise::RegistrationsController
	include ThemesHelper
	# before_filter :configure_sign_up_params, only: [:create]
	protect_from_forgery
	before_filter :configure_account_update_params
	before_action :authenticate_user!, only: [:show, :edit, :update, :destroy]

	# GET /resource/sign_up
	def new
		# Prepare for Views
		@page_title = "Sign Up"
		@page_description = "Sign Up to Join Makerz.io network."
		super
	end

	# POST /resource
	def create
		session[:first_time] = true
		super
	end

	# GET /resource/edit
	def edit
		save_back_url

		# Prepare for Views
		@page_title = "Manage Profile"
		@page_description = "Makerz.io User Profile Management, Accessible after Login."
		@back_url = back_url
		super
	end

	# PUT /resource
	def update
		if @user.provider == "facebook"
				params[:user].delete(:password)
				params[:user].delete(:password_confirmation)
		end

		self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
		prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

		# Processing Tags
		tags_array = params[:user][:tag_object].downcase.split(",").map(&:strip).reject(&:empty?).uniq

		special = "?<>',?[]}{=-)(*&^%$#`~{}"
		regex = /[#{special.gsub(/./){|char| "\\#{char}"}}]/

		tags = tags_array.map do |tag|
			if tag.strip =~regex
				tag_string = tag.strip.gsub(/[^a-zA-Z0-9\-]/,"")
			else
				tag_string = tag.strip
			end

			Tag.find_or_create_by(name: tag_string) if tag_string != ""
		end

		@user.tags = tags

		params[:user].delete(:tag_object)

		# Refresh all free templates
		populate_free_themes_for user: @user, themes: Theme.where(price: 0)

		# Check Subdomain Availabilities
		invalid_subdomain = UserAccount.validate_subdomain(account_update_params[:user_account_attributes][:subdomain], current_user)

		# Update Available User Resources
		resource_updated = @user.update_attributes(account_update_params) unless invalid_subdomain

		# Set current_theme default to the first selection
		set_theme_as_current_theme_for user: @user, theme: @user.themes.first

		yield resource if block_given?

		if invalid_subdomain
			redirect_to edit_user_registration_path, alert: invalid_subdomain
		elsif resource_updated
			if is_flashing_format?
				flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
					:update_needs_confirmation : :updated
				set_flash_message :notice, flash_key
			end
			sign_in resource_name, resource, bypass: true
			respond_with resource, location: after_update_path_for(resource)
		else
			clean_up_passwords resource
			respond_with resource, location: after_update_path_for(resource)
		end


	end

	# DELETE /resource
	# def destroy
	#   super
	# end

	# GET /resource/cancel
	# Forces the session data which is usually expired after sign
	# in to be expired now. This is useful if the user wants to
	# cancel oauth signing in/up in the middle of the process,
	# removing all OAuth session data.
	# def cancel
	#   super
	# end

	# protected


	# You can put the params you want to permit in the empty array.
	# def configure_sign_up_params
	#   devise_parameter_sanitizer.for(:sign_up) << :attribute
	# end

	# You can put the params you want to permit in the empty array.
	def configure_account_update_params
		devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:email, :password, :password_confirmation, :tag_object,:current_password, :user_account_attributes => [:summary, :user_id, :facebook_link, :twitter_link, :linkedIn_link, :google_plus_link, :id, :subdomain, :name]) }
		devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:email, :password, :password_confirmation, :tag_object,:user_account_attributes => [:summary, :user_id, :facebook_link, :twitter_link, :user_url, :linkedIn_link, :google_plus_link, :id, :name]) }
	end

	# The path used after sign up.
	def after_sign_up_path_for(resource)
		edit_user_registration_path
	end

	def after_update_path_for(resource)
		if session[:first_time]
			edit_themes_selectors_path
		else
			edit_user_registration_path
		end
	end

	# The path used after sign up for inactive accounts.
	# def after_inactive_sign_up_path_for(resource)
	#   super(resource)
	# end
end
