class Users::SessionsController < Devise::SessionsController
# before_filter :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  def new

    # Prepare for Views
    @page_title = "Login"
    @page_description = "Login to access Makerz.io makerz network."
    super
  end

  # POST /resource/sign_in
  # def create
  #   super
  # end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected
  def after_sign_in_path_for(resource_or_scope)
    user_accounts_path
  end

  def after_sign_out_path_for(resource_or_scope)
    new_user_session_path
  end

  # You can put the params you want to permit in the empty array.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.for(:sign_in) << :attribute
  # end
end
