class Users::ProfilesController < ApplicationController
	include Users::ProfilesHelper
	protect_from_forgery
	before_action :retrieve_user_from_subdomain
	theme :theme_selector, only: [:show]

	# def index
	# end

	def show
		# Process outputs for views
		@projects = @user_account.user.projects.all
		@user_tags = @user_account.user.tags.all.map { |tag| tag.name }
		@universal_makerz_statement = Tag.generate_makerz_statement(@user_tags)
		@default_picture = default_img_path
	end

	# def new
	# end

	# def create
	# end

	# def edit
	# end

	# def update
	# end

	# def destroy
	# end

	private
	def theme_selector
		ret = select_theme
		redirect_to no_such_profile_path if ret.nil?
		ret
	end

	def not_found
		raise ActionController::RoutingError.new('User Not Found')
	end
end
