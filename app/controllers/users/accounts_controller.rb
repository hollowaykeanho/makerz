class Users::AccountsController < ApplicationController
	include Users::AccountsHelper
	protect_from_forgery
	before_action :authenticate_user!
	before_action :set_user_account, only: [:update]
	before_action :validate_user, only: [:update]

	def index
		clear_back_url by_force: true

		# process output for views
		@page_title = "Your Dashboard"
		@page_description = "Makerz.io User Dashboard, Accessible after Login."
		user = User.find(current_user.id)
		@user_account = user.user_account
	end

	# def show
	# end

	# def new
	# end

	# def create
	# end

	# def edit
	# end

	def update
		if commit_params == "Select Theme"
			if @user_account.update(themes_params)
				if session[:first_time]
					path = user_first_time_done_path
				else
					path = edit_themes_selectors_path
					notification = { notice: 'This user_account was successfully updated.' }
				end
			else
				path = error_500_path
				notification = { alert: 'Something went wrong with the selection.' }
			end
		else
			path = error_500_path
			notification = { alert: 'Something went wrong with server.' }
		end

		# Process for output
		notification = {} if notification.nil?
		redirect_to path, notification
	end

	# def destroy
	# end

	private
		def set_user_account
			@user = User.find_by(id: current_user.id)
			@user_account = @user.user_account
		end

		# Never trust parameters from the scary internet, only allow the white list through.
		def themes_params
			params.require(:user_account).permit(:current_theme)
		end

		def commit_params
			params.permit(:commit)[:commit]
		end
end
