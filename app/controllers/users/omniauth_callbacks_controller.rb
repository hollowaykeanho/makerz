class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    # You need to implement the method below in your model (e.g. app/models/user.rb)
    @user = User.from_omniauth(request.env["omniauth.auth"])

    if @user.persisted?
      session[:first_time] = true if @user.sign_in_count == 0

      sign_in @user, :event => :authentication #this will throw if @user is not activated
      set_flash_message(:notice, :success, :kind => "Facebook") if is_navigational_format?
      after_omniauth_callback_path_for(@user)
    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      redirect_to facebook_signup_without_email_faq_path, alert: "I'm sorry, For sign up with facebook, we require your email information. Please follow the instructions below: "
    end
  end

  def after_omniauth_callback_path_for(resource)
      if session[:first_time]
        redirect_to edit_user_registration_path(@resource)
      else
        redirect_to user_accounts_path
      end
  end
end