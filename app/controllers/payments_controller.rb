class PaymentsController < ApplicationController
	protect_from_forgery except: [:hook]
	skip_before_filter  :verify_authenticity_token, :authenticate_user!

	def hook
  	params.permit!
  	status = params[:payment_status]
    @order = Order.find params[:invoice]
    if status == "Completed" && Payment.where(transaction_id: params[:txn_id]) == [] && params[:secret] == "#{Rails.application.secrets.paypal_secret}" && params[:receiver_email] == "#{Rails.application.secrets.paypal_email}" && params[:mc_gross] == sprintf("%.2f", @order.subtotal.to_s) && params[:mc_currency] == "#{Rails.application.secrets.paypal_currency}"
      @order.update_attributes(order_status_id: 3)
      payment = @order.user.payments.create(notification_params: params, price: params[:mc_gross],status: status, transaction_id: params[:txn_id], purchased_at: Time.now)
      @order.order_items.each {|x| payment.user.user_themes.create(theme_id: x.theme.id, payment_id: payment.id) }
      @order.order_items.destroy_all
    end
    render nothing: true
  end
end
