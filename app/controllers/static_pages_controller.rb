class StaticPagesController < ApplicationController

	def terms_and_conditions
		# Process output for views
		@page_title = "Terms of Services"
		@page_description = "Understand Makerz.io Terms of Services before use."
	end

	def explained
		# Process output for views
		@page_title = "Explained"
		@page_description = "Know in detailed what is Makerz.io about. See through the world of makerz."
	end

	def privacy
		# Process output for views
		@page_title = "Privacy"
		@page_description = "Understand Makerz.io Privacy Agreement Before Use."
	end

	def contact_us
		# Process output for views
		@page_title = "Contact Us"
		@page_description = "Contact Makerz.io for feedbacks and any inquires."
	end
end
