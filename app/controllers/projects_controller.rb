class ProjectsController < ApplicationController
	include ProjectsHelper
	protect_from_forgery
	before_action :authenticate_user!
	before_action :validate_project_existance, only: [:edit, :show, :update, :destroy]
	before_action :validate_owner, only: [:edit, :update, :destroy]
	# before_action :validate_user_confirmation

	def index
		# Process output for views
		@page_title = "Project List"
		@page_description = "This is Makerz.io Project List, Accessible after Login."

		@projects = current_user.projects || []
		@default_picture = default_img_path
	end

	def new
		save_back_url

		# Process output for views
		@page_title = "New Project"
		@page_description = "This is Makerz.io New Project Creation for User, Accessible after Login."

		@project = current_user.projects.new
		@project.photos.build
		@project.videos.build
		@back_url = back_url
	end

	def create
		tags = split_tags(params[:project][:project_tags_object])

		params[:project].delete(:project_tags_object)

		@project = current_user.projects.new(project_params)

		@project.tags = tags

		if @project.save
			redirect_to projects_url, :notice => "Your project has been created"
		else
			render :new
		end

	end

	def edit
		@project.photos.build if has_no_contents? @project.photos
		@project.videos.build if has_no_contents? @project.videos

		# Process output for views
		@page_title = "Edit Project"
		@page_description = "This is Makerz.io Edit Project Creation for User, Accessible after Login."
			# @project is already handled by :validate_project_existance
	end

	def update
		tags = split_tags(params[:project][:project_tags_object])

		params[:project].delete(:project_tags_object)

		@project.attributes =  project_params
		@project.tags = tags

		if @project.save
			redirect_to projects_url, notice: 'This project was successfully updated.'
		else
			render :edit
		end
	end

	def show
		@owner = @project.user.user_account

		# Process output for views
		@page_title = "#{ @owner.name } - #{ @project.title }"
		@page_description = "This is Makerz.io Edit Project Creation for User, Accessible after Login."
		@project_tags = @project.tags.all.map { |tag| tag.name }.sort
		@owner_tags = @project.user.tags.all.map { |tag| tag.name }
	end

	def destroy
		@project.destroy
		redirect_to projects_url, :notice => "Your project was successfully deleted"
	end

	private
		def split_tags(tag_string)
			tags_array = tag_string.downcase.split(",").map(&:strip).reject(&:empty?).uniq

			tags_array.map do |tag|
				if tag.strip =~ Tag::TAG_REGEX
					tag_string = tag.strip.gsub(/[^a-zA-Z0-9\-]/,"")
				else
					tag_string = tag.strip
				end
				Tag.find_or_create_by(name: tag_string) if tag_string != ""
			end


		end

		# Never trust parameters from the scary internet, only allow the white list through.
		def project_params
			params.require(:project).permit(:user_id, :title, :motivation, :description, :additional, :method, :project_tags_object,
																			photos_attributes: [:id, :url], videos_attributes: [:id, :url])
		end
end
