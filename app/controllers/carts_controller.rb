class CartsController < ApplicationController
	protect_from_forgery
	before_action :authenticate_user!

	def show

		# Process Output for Views
		@page_title = "Shopping Cart"
		@page_description = "This is Makerz.io user shopping cart."

		@js_custom_turbolink = { 'turbolinks-track' => true, "turbolinks-eval" => false }
		@order_items = current_order.order_items
		@subtotal = current_order.subtotal

		@payment_return_path = session[:first_time] ? edit_themes_selectors_path : themes_path
	end

end