class OrderItemsController < ApplicationController
	protect_from_forgery
	before_action :authenticate_user!
	before_action :set_order, only: [ :create, :update, :destroy]

  def create
    if current_user.user_themes.where(theme_id: params[:order_item][:theme_id]) != []
      flash[:notice] = "You have bought this theme before"
    elsif @order.order_items.where(theme_id: order_item_params[:theme_id]) == []
      flash.clear
      @order_item = @order.order_items.new(order_item_params)
      @order.save
    else
      flash[:notice] = "You have order this theme before"
    end
    session[:order_id] = @order.id
  end

  def update
    @order_item = @order.order_items.find(params[:id])
    @order_item.update_attributes(order_item_params)
    @order_items = @order.order_items
  end

  def destroy
    @order_item = @order.order_items.find(params[:id])
    @order_item.destroy
    @order_items = @order.order_items
  end

  private

  def set_order
  	@order = current_order
  end


  def order_item_params
    params.require(:order_item).permit(:quantity, :theme_id, :category)
  end
end
