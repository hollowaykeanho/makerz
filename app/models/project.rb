class Project < ActiveRecord::Base
	belongs_to :user
	has_many :project_tags
	has_many :tags, through: :project_tags
	has_many :photos
	has_many :videos

	validates :title, presence: true, :uniqueness => { :scope => :user_id, message: "You have an existing project with the same name" }
	validates :motivation, presence: { message: "Motivation can't be empty. Let us know more about your project. Please key in some" }
	validates :description, presence: { message: "Description can't be empty. Potential users search based on the description. Please key in some" }

	accepts_nested_attributes_for :photos, allow_destroy: true, reject_if: proc { |attributes| attributes.all? {|k,v| v.blank? } }
	accepts_nested_attributes_for :videos, allow_destroy: true, reject_if: proc { |attributes| attributes.all? {|k,v| v.blank? } }

	filterrific(
		default_filter_params: { sorted_by: 'created_at_desc' },
		available_filters: [
			:sorted_by,
			:search_query,
			:with_tag_id,
			:with_created_at_gte
		]
	)

	self.per_page = 20

	scope :search_query, lambda { |query|
		return nil if query.blank?
		terms = query.downcase.split(/\s+/)
		terms = terms.map { |e|
			(e.gsub('*', '%') + '%').gsub(/%+/, '%')
		}
		num_or_conditions = 3
		where(
			terms.map {
				or_clauses = [
					"LOWER(title) LIKE ?",
					"LOWER(description) LIKE ?",
					"LOWER(additional) LIKE ?"
				].join(' OR ')
				"(#{ or_clauses })"
			}.join(' AND '),
			*terms.map { |e| p [e] * num_or_conditions }.flatten)
	}

	scope :sorted_by, lambda { |sort_option|
		direction = (sort_option =~ /desc$/) ? "DESC" : "ASC"

		case sort_option.to_s
		when /^created_at_/
			order(created_at: direction)
		when /^name_/
			order(title: direction)
		when /^tag_name_/
			includes(:tags).order("tags.name #{ direction }")
		else
			raise(ArgumentError, "Invalid sort option: #{ sort_option.inspect }")
		end
	}

	scope :with_created_at_gte, lambda { |ref_date|
		where('created_at >= ?', ref_date)
	}

	scope :with_tag_id, lambda { |tag_ids|
		temp = tag_ids.split(/\W+/)
		joins(:tags).select('projects.*').where(tags: { name: [*temp] }).group('projects.id')
	}

	def project_tags_object
		tags.pluck(:name).join(", ")
	end

	def self.options_for_sorted_by
		[
			['Name (a-z)', 'name_asc'],
			['Name (z-a)', 'name_desc'],
			['Registration date (newest first)', 'created_at_desc'],
			['Registration date (oldest first)', 'created_at_asc'],
			['Tag (a-z)', 'tag_name_asc'],
			['Tag (z-a)', 'tag_name_desc']
		]
	end

	def decorated_created_at
		created_at.to_date.to_s(:long)
	end
end
