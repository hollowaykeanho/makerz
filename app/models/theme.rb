class Theme < ActiveRecord::Base
	has_many :user_tags
	has_many :users, through: :user_tags
	has_many :order_items

	default_scope { where(active: true) }
end
