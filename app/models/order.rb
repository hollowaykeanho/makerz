class Order < ActiveRecord::Base
  belongs_to :order_status
  has_many :order_items
  belongs_to :user
  before_create :set_order_status
  before_save :update_subtotal

  def subtotal
    order_items.collect { |oi| oi.valid? ? (oi.quantity * oi.unit_price) : 0 }.sum
  end

  def paypal_url(return_path, user)
    values = {
        :business => "#{Rails.application.secrets.paypal_email}",
        :cmd => "_cart",
        :upload => 1,
        :return => "#{Rails.application.secrets.app_host}#{return_path}",
        :invoice => id,
        :currency_code => "#{Rails.application.secrets.paypal_currency}",
        :notify_url => "#{Rails.application.secrets.app_host}/hook?secret=#{Rails.application.secrets.paypal_secret}",
        :cert_id => "#{Rails.application.secrets.paypal_cert_id}"
    }
    self.order_items.each_with_index do |item, index|
      values.merge!({
        "amount_#{index + 1}" => item.unit_price,
        "item_name_#{index + 1}" => item.theme.name,
        "quantity_#{index + 1}" => item.quantity
      })
    end
    encrypt_for_paypal(values)
  end
private
  PAYPAL_CERT_PEM = File.read("#{Rails.root}/certs/paypal_cert.pem")
  APP_CERT_PEM = File.read("#{Rails.root}/certs/app_cert.pem")
  APP_KEY_PEM = File.read("#{Rails.root}/certs/app_key.pem")

  def encrypt_for_paypal(values)
    signed = OpenSSL::PKCS7::sign(OpenSSL::X509::Certificate.new(APP_CERT_PEM), OpenSSL::PKey::RSA.new(APP_KEY_PEM, ''), values.map { |k, v| "#{k}=#{v}" }.join("\n"), [], OpenSSL::PKCS7::BINARY)
    OpenSSL::PKCS7::encrypt([OpenSSL::X509::Certificate.new(PAYPAL_CERT_PEM)], signed.to_der, OpenSSL::Cipher::Cipher::new("DES3"), OpenSSL::PKCS7::BINARY).to_s.gsub("\n", "")
  end

  def set_order_status
    self.order_status_id = 1
  end

  def update_subtotal
    self[:subtotal] = subtotal
  end

end
