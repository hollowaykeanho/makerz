class User < ActiveRecord::Base
	has_one :user_account, dependent: :destroy, autosave: true
	has_many :user_tags
	has_many :tags, through: :user_tags
	has_many :user_themes
	has_many :themes, through: :user_themes
	has_many :projects
	has_many :payments
	has_many	:orders

  accepts_nested_attributes_for :user_account, :allow_destroy => true

  accepts_nested_attributes_for :projects, :allow_destroy => true, :reject_if => lambda { |a| a[:title].blank? }

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
          :lockable, :timeoutable, :confirmable, :omniauthable, :omniauth_providers => [:facebook]

  def tag_object
  	tags.pluck(:name).join(", ")
  end

  def self.from_omniauth(auth)
	  where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
	    user.email = auth.info.email
	    user.password = Devise.friendly_token[0,20]
	    user.skip_confirmation!
	    # user.image = auth.info.image # assuming the user model has an image
	  end
	end

	def self.find_for_facebook_oauth(access_token, signed_in_resource=nil)
		data = access_token['info']['email']
		if user = User.find_by_email(data["email"])
		  user
		else # Create a user with a stub password.
		  user = User.new(:email => data["email"], :password => Devise.friendly_token[0,20])
		  user.skip_confirmation!
		  user.save
		 end
	end

	def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  def send_devise_notification(notification, *args)
    devise_mailer.send(notification, self, *args).deliver_later
  end

end
