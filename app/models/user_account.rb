class UserAccount < ActiveRecord::Base
	belongs_to :user

	URL_REGEX = /(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?/ix

	validates :facebook_link, format: {with: URL_REGEX, message: "Facebook url is invalid"}, if: Proc.new { |a| a.facebook_link.present? }
	validates :twitter_link, format: {with: URL_REGEX, message: "Twitter url is invalid"}, if: Proc.new { |a| a.twitter_link.present? }
	validates :linkedIn_link, format: {with: URL_REGEX, message: "LinkedIn url is invalid"}, if: Proc.new { |a| a.linkedIn_link.present? }
	validates :google_plus_link, format: {with: URL_REGEX, message: "Google+ url is invalid"}, if: Proc.new { |a| a.google_plus_link.present? }

	validates :picture_url, format: {with: URL_REGEX, message: "Photo Link is Invalid"}, if: Proc.new { |a| a.picture_url.present? }
	validates :subdomain, presence: false, uniqueness: true
	validates :current_theme, numericality: true, allow_nil: true, allow_blank: true
	validates_presence_of :name

	validates_format_of :subdomain, with: /\A[a-z0-9_]+\z/,
																	message: "must be lowercase alphanumerics only"

	validates_length_of :subdomain, maximum: 32,
																	message: "exceeds maximum of 32 characters"

	validates_length_of :subdomain, minimum: 5,
																	message: "must have minimum of 4 characters"

	validates_exclusion_of :subdomain, in: BLACKLIST,message: "is not available. Please enter another choice."

	def self.validate_subdomain(subdomain, current_user)
		# Check subdomain occupation from current database
		user_account = UserAccount.find_by(subdomain: subdomain)
		if !user_account.nil? && user_account.user != current_user
			return "#{subdomain} has already been taken."
		end

		# Check if subdomain is blacklisted
		return "#{subdomain} is not available. Please enter another one." if BLACKLIST.include? subdomain
		nil
	end
end
