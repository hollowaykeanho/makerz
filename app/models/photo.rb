class Photo < ActiveRecord::Base
	URL_REGEX = /(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?/ix
	belongs_to :project

	validates :url, format: {with: URL_REGEX, message: "Photo Link is Invalid"}, if: Proc.new { |a| a.url.present? }
end
