class Tag < ActiveRecord::Base
	has_many :user_tags
	has_many :users, through: :user_tags

	has_many :project_tags
	has_many :projects, through: :project_tags

	SPECIAL = "?<>',?[]}{=-)(*&^%$#`~{}"
	TAG_REGEX = /[#{SPECIAL.gsub(/./){|char| "\\#{char}"}}]/

	def self.options_for_select
	  self.order('LOWER(name)').map { |e| [e.name, e.id] }
	end

	def self.generate_makerz_statement(user_tag)
		statement = "I'm a Makerz who makes stuff"
		unless user_tag.nil? || user_tag.blank?
			statement << " related to "
			user_tag.each_with_index do |tag, index|
				if index == user_tag.size - 1 && user_tag.size != 1
					statement << " and "
				elsif index > 0
					statement << ", "
				end
				statement << tag
			end
		else
			statement << "."
		end
		statement
	end

end
