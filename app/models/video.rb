class Video < ActiveRecord::Base
	URL_REGEX = /(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?/ix
	VIDEO_URLS = {youtube:	{	tag: /(?:youtube(?:-nocookie)?\.com\/(?:[^\/\n\s]+\/\S+\/|(?:v|e(?:mbed)?)\/|\S*?[?&]v=)|youtu\.be\/)/,
														filter: /(?:youtube(?:-nocookie)?\.com\/(?:[^\/\n\s]+\/\S+\/|(?:v|e(?:mbed)?)\/|\S*?[?&]v=)|youtu\.be\/)([a-zA-Z0-9_-]{11})/,
														universal_url: "https://www.youtube.com/embed/"
								},
								vimeo: 		{ tag: /https?:\/\/ ?[www\.]|[player\.]?(vimeo\.com)\[?]*/,
														filter: /https?:\/\/ ?[www\.]|[player\.]?vimeo\.com\/[[a-z]*\/]*([0-9]{6,11})[?]?.*/,
														universal_url: "https://player.vimeo.com/video/"
								}
							}

	belongs_to :project

	validates :url, format: {with: URL_REGEX, message: "Video Link is Invalid"}, if: Proc.new { |a| a.url.present? }

	before_save :process_video_url


	def process_video_url
		VIDEO_URLS.each do |host, regex|
			if self.url.match(regex[:tag])
				self.url =  regex[:universal_url] + self.url.match(regex[:filter])[1]
				break
			end
		end
	end

end
