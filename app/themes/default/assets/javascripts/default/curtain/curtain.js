jQuery(document).ready(function($){
	//change this value if you want to change the speed of the scale effect
	var	scaleSpeed = 0.3,
	//change this value if you want to set a different initial opacity for the .ctn-half-window
		boxShadowOpacityInitialValue = 0.7,
		animating = false;

	//check for trigger
	var MQ = window.getComputedStyle(document.querySelector('body'), '::before').getPropertyValue('content').replace(/"/g, "").replace(/'/g, "");
	$(window).on('resize', function(){
		MQ = window.getComputedStyle(document.querySelector('body'), '::before').getPropertyValue('content').replace(/"/g, "").replace(/'/g, "");
	});

	//bind the animation to the window scroll event
	triggerAnimation();
	$(window).on('scroll', function(){
		triggerAnimation();
	});

	//bind all navigation inputs
	$('.ctn-prev').on('click', function(event){
		if(MQ == 'ctn-animate') {
			prevSection();
			event.preventDefault();
		}
	});
	$('.ctn-next').on('click', function(event){
		if(MQ == 'ctn-animate') {
			nextSection();
			event.preventDefault();
		}
	});
	$(document).keydown(function(event){
		if( event.which=='38' ) {
			prevSection();
			event.preventDefault();
		} else if( event.which=='40' ) {
			nextSection();
			event.preventDefault();
		}
	});

	function triggerAnimation() {
		if(MQ == 'ctn-animate') {
			(!window.requestAnimationFrame) ? animateSection() : window.requestAnimationFrame(animateSection);
		} else {
			$('.section').find('.ctn-full-window').removeAttr('style').find('.ctn-half-window').removeAttr('style');
		}
		checkNavigation();
	}

	function animateSection () {
		var scrollTop = $(window).scrollTop(),
			windowHeight = $(window).height(),
			windowWidth = $(window).width();

		$('.section').each(function(){
			var section = $(this),
				offset = scrollTop - section.offset().top,
				scale = 1,
				translate = windowWidth/2+'px',
				opacity,
				boxShadowOpacity;

			if( offset >= -windowHeight && offset <= 0 ) {
				//move the two .ctn-half-window toward the center - no scale/opacity effect
				scale = 1,
				opacity = 1,
				translate = (windowWidth * 0.5 * (- offset/windowHeight)).toFixed(0)+'px';

			} else if( offset > 0 && offset <= windowHeight ) {
				//the two .ctn-half-window are in the center - scale the .ctn-full-window element and reduce the opacity
				translate = 0+'px',
				scale = (1 - ( offset * scaleSpeed/windowHeight)).toFixed(5),
				opacity = ( 1 - ( offset/windowHeight) ).toFixed(5);

			} else if( offset < -windowHeight ) {
				//section not yet visible
				scale = 1,
				translate = windowWidth/2+'px',
				opacity = 1;

			} else {
				//section not visible anymore
				opacity = 0;
			}

			boxShadowOpacity = parseInt(translate.replace('px', ''))*boxShadowOpacityInitialValue/20;

			//translate/scale section blocks
			scaleBlock(section.find('.ctn-full-window'), scale, opacity);

			var leftWindowBlock = ( section.find('.ctn-left-window-wrap-in') ) ? '-': '+';
			var rightWindowBlock = ( section.find('.ctn-right-window-wrap-in') ) ? '+': '-';

			if(section.find('.ctn-half-window')) {
				translateBlock(section.find('.ctn-half-window').eq(0), leftWindowBlock+translate, boxShadowOpacity);
				translateBlock(section.find('.ctn-half-window').eq(1), rightWindowBlock+translate, boxShadowOpacity);
			}
			//this is used to navigate through the sections
			( offset >= 0 && offset < windowHeight ) ? section.addClass('is-visible') : section.removeClass('is-visible');
		});
	}

	function translateBlock(elem, value, shadow) {
		var position = Math.ceil(Math.abs(value.replace('px', '')));

		if( position >= $(window).width()/2 ) {
			shadow = 0;
		} else if ( position > 20 ) {
			shadow = boxShadowOpacityInitialValue;
		}

		elem.css({
			'-moz-transform': 'translateX(' + value + ')',
			'-webkit-transform': 'translateX(' + value + ')',
			'-ms-transform': 'translateX(' + value + ')',
			'-o-transform': 'translateX(' + value + ')',
			'transform': 'translateX(' + value + ')',
			'box-shadow' : '0px 0px 40px rgba(0,0,0,'+shadow+')'
		});
	}

	function scaleBlock(elem, value, opac) {
		elem.css({
			'-moz-transform': 'scale(' + value + ')',
			'-webkit-transform': 'scale(' + value + ')',
			'-ms-transform': 'scale(' + value + ')',
			'-o-transform': 'scale(' + value + ')',
			'transform': 'scale(' + value + ')',
			'opacity': opac
		});
	}

	function nextSection() {
		if (!animating) {
			if ($('.section.is-visible').next().length > 0) smoothScroll($('.section.is-visible').next());
		}
	}

	function prevSection() {
		if (!animating) {
			var prevSection = $('.section.is-visible');
			if(prevSection.length > 0 && $(window).scrollTop() != prevSection.offset().top) {
				smoothScroll(prevSection);
			} else if(prevSection.prev().length > 0 && $(window).scrollTop() == prevSection.offset().top) {
				smoothScroll(prevSection.prev('.section'));
			}
		}
	}

	function checkNavigation() {
		( $(window).scrollTop() < $(window).height()/2 ) ? $('.ctn-vertical-nav .ctn-prev').addClass('inactive') : $('.ctn-vertical-nav .ctn-prev').removeClass('inactive');
		( $(window).scrollTop() > $(document).height() - 3*$(window).height()/2 ) ? $('.ctn-vertical-nav .ctn-next').addClass('inactive') : $('.ctn-vertical-nav .ctn-next').removeClass('inactive');
	}

	function smoothScroll(target) {
		animating = true;
		$('body,html').animate({'scrollTop': target.offset().top}, 500, function(){ animating = false; });
	}
});