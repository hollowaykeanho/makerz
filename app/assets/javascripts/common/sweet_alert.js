$(document).on('ready page:load', function(){
	data = $('#flash-message').data();
	if (null != data) {
		if ('success' == data.notice) {
			swal("Here's a message!", data.msg, data.notice);
		} else {
			swal('Uh Oh!', data.msg, data.notice);
		}
	}
});
