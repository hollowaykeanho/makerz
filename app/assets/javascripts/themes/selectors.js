$(document).on('ready page:load', function(){
	refreshThemeOption();
	$(".theme-option").click(function(){
		refreshThemeOption();
	})
})

function refreshThemeOption() {
	$('.theme-option').removeClass("active");
	var selected = $(".invisible-input-radio:checked").parent();
	selected.addClass("active");
}