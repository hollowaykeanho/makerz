$(document).on('ready page:load', function(){
	shapeshiftProjectLoad();

	$("img").load(function(){
		shapeshiftProjectLoad();
	});
});


function shapeshiftProjectLoad() {
	$('#shapeshift-list').shapeshift({
		enableDrag: false,
		enableCrossDrop: false,
		minColumns: 1
	});
}

function projectPopup(project_id) {
	$('#modal-show-project-' + project_id).modal('show');
};