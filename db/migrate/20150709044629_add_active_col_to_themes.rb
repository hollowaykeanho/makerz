class AddActiveColToThemes < ActiveRecord::Migration
  def change
  	add_column :themes, :active, :boolean
  end
end
