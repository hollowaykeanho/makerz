class RemoveOrderIdFromPayments < ActiveRecord::Migration
  def change
    remove_column :payments, :order_id, :integer
  end
end
