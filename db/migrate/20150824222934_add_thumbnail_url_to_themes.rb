class AddThumbnailUrlToThemes < ActiveRecord::Migration
  def change
  	add_column :themes, :thumbnail_url, :string
  end
end
