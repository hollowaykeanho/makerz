class ShiftNameFromUsersToUserAccounts < ActiveRecord::Migration
  def change
  	add_column :user_accounts, :name, :string
  	remove_column :users, :name
  end
end
