class AddMotivationToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :motivation, :string
  end
end
