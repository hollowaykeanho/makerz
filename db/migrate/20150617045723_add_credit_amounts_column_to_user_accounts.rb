class AddCreditAmountsColumnToUserAccounts < ActiveRecord::Migration
  def change
  	add_column :user_accounts, :credit_amounts, :decimal
  end
end
