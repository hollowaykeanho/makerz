class AddUserUrlToUserAccounts < ActiveRecord::Migration
  def change
    add_column :user_accounts, :user_url, :string
  end
end
