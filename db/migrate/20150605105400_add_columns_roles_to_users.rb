class AddColumnsRolesToUsers < ActiveRecord::Migration
  def change
    add_column :users, :roles, :string
    add_column :users, :credit_amounts, :decimal
  end
end
