class RenameColumnUserAccountsUserUrlToSubdomain < ActiveRecord::Migration
  def change
  	rename_column :user_accounts, :user_url, :subdomain
  end
end
