class AddPictureUrlToUserAccounts < ActiveRecord::Migration
	def change
		add_column :user_accounts, :picture_url, :string
	end
end
