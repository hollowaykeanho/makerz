class AddPaymentIdToUserThemes < ActiveRecord::Migration
  def change
    add_column :user_themes, :payment_id, :integer
  end
end
