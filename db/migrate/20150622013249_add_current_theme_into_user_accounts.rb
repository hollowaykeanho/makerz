class AddCurrentThemeIntoUserAccounts < ActiveRecord::Migration
	def change
		add_column :user_accounts, :current_theme, :integer
	end
end
