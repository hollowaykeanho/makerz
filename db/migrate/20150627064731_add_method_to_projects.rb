class AddMethodToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :method, :text
  end
end
