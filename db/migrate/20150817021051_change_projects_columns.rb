class ChangeProjectsColumns < ActiveRecord::Migration
	def change
		change_column :projects, :description, :text, null: false
		change_column :projects, :motivation, :string, null: false
		change_column :projects, :title, :string, null: false
	end
end
