class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :user_id
      t.string :name
      t.decimal :price, precision: 12, scale: 3
      t.text :notification_params
      t.string :status
      t.string :transaction_id
      t.datetime :purchased_at
      t.integer :order_id

      t.timestamps null: false
    end
  end
end
