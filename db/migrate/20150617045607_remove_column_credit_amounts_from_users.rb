class RemoveColumnCreditAmountsFromUsers < ActiveRecord::Migration
  def change
  	remove_column :users, :credit_amounts
  end
end
