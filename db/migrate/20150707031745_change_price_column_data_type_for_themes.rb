class ChangePriceColumnDataTypeForThemes < ActiveRecord::Migration
  def change
  	change_column :themes, :price, :decimal, precision: 12, scale: 3
  end
end
