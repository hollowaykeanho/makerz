class ChangeSummaryInProjects < ActiveRecord::Migration
  def change
  	rename_column :projects, :summary, :additional
  	change_column :projects, :additional, :text
  end
end
