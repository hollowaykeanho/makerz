class CreateUserAccounts < ActiveRecord::Migration
  def change
    create_table :user_accounts do |t|
      t.integer :user_id
      t.text :summary
      t.string :facebook_link
      t.string :twitter_link
      t.string :linkedIn_link
      t.string :google_plus_link

      t.timestamps null: false
    end
  end
end
