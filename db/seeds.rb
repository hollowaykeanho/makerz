# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Theme.delete_all
theme_list = [{name: "hosting_store", category: "", price: 0, active: true, thumbnail_url: "https://lh3.googleusercontent.com/7772l7f5wXWiVM8FNH61lV_mHx4GmoVZZhwJ2sboeSZc=w190-h127-no"},
							{name: "magister", category: "", price: 0, active: true, thumbnail_url: "https://lh3.googleusercontent.com/EVsOoITNmXvWF_1ir6ryRAGriOjAuUv8X5Wp64WRqRgx=w190-h127-no"},
							{name: "strata", category: "", price: 0, active: true, thumbnail_url: "https://lh3.googleusercontent.com/zBw0_w6KU4J1dKJWIwrQGyEky6xcNgVmKvRtGYVnEQQD=w190-h127-no"}]

theme_list.each do |theme|
	Theme.find_or_create_by(theme)
end

OrderStatus.delete_all
OrderStatus.create! id: 1, name: "In Progress"
OrderStatus.create! id: 2, name: "Placed"
OrderStatus.create! id: 3, name: "Shipped/Success"
OrderStatus.create! id: 4, name: "Cancelled"


# Order.delete_all

# 80.times do |x|
# 	Order.create(subtotal: 100.000)
# end

# 10.times do |x|
# 	a = Project.create(user_id: 1, title: "project_#{x}", description: "#{x}_honey")
# 	a.tags.create(name: "banana_#{x}")
# end


p "finish"