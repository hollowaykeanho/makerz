Rails.application.routes.draw do

  # =======================================
  #shopping cart
  # =======================================
  resources :themes, only: [:index]
  resource :cart, only: [:show]
  resources :order_items, only: [:create, :update, :destroy, :show]
  post "/hook" => "payments#hook"

  # =======================================
  # User Routes
  # =======================================
    # Devise
  devise_for :users, controllers: {sessions: "users/sessions", omniauth_callbacks: "users/omniauth_callbacks", registrations: 'users/registrations', passwords: 'users/passwords'}
    # Account
  namespace :users, as: "user" do
    resources :accounts, only: [:index, :update]
  end

  resource :user, only: [:edit, :update]

    # User Sign Up related Documents
  get 'user/first_time_done', to: 'welcome#first_time_done', as: 'user_first_time_done'
  get "/facebook_signup_without_email_faq", to: "welcome#email"

  # =======================================
  # Projects
  # =======================================
  get '/projects/search' , to: "searches#project_search", as: 'project_search'
  resources :projects

  # =======================================
  # Themes
  # =======================================
  namespace :themes do
    resource :selectors, only: [:edit]
  end

  # =======================================
  # Static Pages
  # =======================================
  get "/terms_of_services", to: "static_pages#terms_and_conditions", as: "terms_of_services"
  get "/privacy", to: "static_pages#privacy", as: "privacy"
  get "/explained", to: "static_pages#explained", as: "explained"
  get "/contact_us", to: "static_pages#contact_us", as: "contact_us"
  # =======================================
  # System
  # =======================================
    # Initiate Subdomain Search
  constraints(Subdomain) do
    get '/',  to: "users/profiles#show", as: 'custom_root'
  end
    # Fall Back to Root Page
  root 'welcome#index'

    # Error
  get '/404', to: 'system/errors#error_404', as: 'error_404', via: :all
  get '/422', to: 'system/errors#error_422', as: 'error_422', via: :all
  get '/500', to: 'system/errors#error_500', as: 'error_500', via: :all
  get '/no_such_project', to: 'system/errors#error_no_such_project', as: 'error_no_such_project', via: :all
  get '/no_such_profile', to: 'system/errors#no_such_profile', as: 'no_such_profile', via: :all
end
